import javax.swing.*;
import java.awt.Graphics;
import java.awt.GridLayout;
import java.awt.Component;
import java.awt.Dimension;

class GridPan extends JPanel{

	int width,height,ro,co;
	int padding = 10;
	
	GridPan(int w, int h, int r, int c){
		setSize(width = w, height = h);
		ro = r;
		co = c;
	}
	
	public void paint(Graphics g){
		int i;
		int mwidth = width;
		int mheight = height;
		
		int RH = height/ro;
		int RW = width/co;
		
		for(i=0; i<=ro; i++){
			g.drawLine(10, i*RH+10, mwidth+10, i*RH+10);
		}
		for(i=0; i<=co; i++){
			g.drawLine(i*RW+10, 10, i*RW+10, mheight+10);
		}
	}	
}


public class GridandArray {
	
	
	
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		JFrame fr = new JFrame();
		fr.setTitle("Comp229 Window");
		fr.setSize(1280, 720);
		fr.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		//fr.pack();
		fr.setVisible(true);
		GridPan test = new GridPan(720,720,20,20);
		fr.add(test);
	}

}
