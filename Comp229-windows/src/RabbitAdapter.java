import bos.*;
import bos.Rabbit;
import java.awt.*;
import java.util.Optional;

public class RabbitAdapter extends Character {
    private Rabbit adaptee;

    public RabbitAdapter(Acell location) {
        super(location, null);
        adaptee = new Rabbit();
        display = Optional.of(new Color(0.6f,0.6f,0.6f));
    }

    public RelativeMove aiMove(Stage stage){
        switch (adaptee.nextMove()){
            case 0:
                return new MoveDown(stage.grid, this);
            case 1:
                return new MoveUp(stage.grid, this);
            case 2:
                return new MoveLeft(stage.grid, this);
            case 3:
                return new MoveRight(stage.grid, this);
        };
        return new MoveRandomly(stage.grid, this);
    }
}
