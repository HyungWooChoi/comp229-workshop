import java.util.ArrayList;

import bos.GameBoard;

public class KeyObservable extends ArrayList<KeyObserver>{
	public void register(KeyObserver Ko){
		add(Ko);
	}
	
	public void notifyAll(char c, GameBoard<Acell> gb){
		this.forEach((KeyObserver ko)->ko.notify(c,gb));
		/**
		 * ^
		 * =
		 * for(int i =0; i< this.size();i++){
		 * 	this.get(i).notify(c,gb)
		 * }
		 */
	}
}
