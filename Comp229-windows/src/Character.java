import java.awt.*;
import java.util.Map;
import java.util.Optional;
import java.util.List;

import bos.GamePiece;
import bos.RelativeMove;

public abstract class Character implements GamePiece<Acell> {
    Optional<Color> display;
    Acell location;
    Behaviour behaviour;

    public Character(Acell location, Behaviour behaviour){
        this.location = location;
        this.display = Optional.empty();
        this.behaviour = behaviour;
    }

    public  void paint(Graphics g){
        if(display.isPresent()) {
            g.setColor(display.get());
            g.fillOval(location.x + location.width / 4, location.y + location.height / 4, location.width / 2, location.height / 2);
        }
    }

    public void setLocationOf(Acell loc){
        this.location = loc;
    }

    public Acell getLocationOf(){
        return this.location;
    }

    public void setBehaviour(Behaviour behaviour){
        this.behaviour = behaviour;
    }

    public RelativeMove aiMove(Stage stage){
        return behaviour.chooseMove(stage, this);
    }
}
